/*
 * Copyright 2022 Alexander D Stokes
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef BASE32_h
#define BASE32_h


uint8_t fromBase32(uint8_t j);
uint8_t getBase32(uint8_t t);

// RFC4648 base32

// ABCDEFGHIJKLMNOPQRSTUVWXYZ234567

inline uint8_t fromBase32(uint8_t j) {
  if (j > 0x60) j -= 0x20;

  if (j > 0x40) j -= 0x41;
  if (j > 0x2F) j -= 0x18;

  return j & 0x1f;
}


inline uint8_t getBase32(uint8_t t) {
  t &= 0x1f;

  if ( t < 0x19) t += 0x41; 
  else t += 0x18;

  return t;
}

#endif // BASE32_h