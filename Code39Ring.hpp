/*
 * Copyright 2022 Alexander D Stokes
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE39RING_h
#define CODE39RING_h

const uint16_t BarcodeSymbol[] = {
  0xA3BA, //   0 
  0xE8AE, //   1 
  0xB8AE, //   2 
  0xEE2A, //   3 
  0xA3AE, //   4 
  0xE8EA, //   5 
  0xB8EA, //   6 
  0xA2EE, //   7 
  0xE8BA, //   8 
  0xB8BA, //   9 
  0xEA2E, //   A 
  0xBA2E, //   B 
  0xEE8A, //   C 
  0xAE2E, //   D 
  0xEB8A, //   E 
  0xBB8A, //   F 
  0x8BBA, //   *       
  0x0000, //   NULL
};


class Code39Ring {
public:
  Code39Ring();
  
  void display(uint8_t code[10]);
  uint8_t get(uint32_t loopCount);
  
private:

  uint16_t m_displayBuffer[18];
};


inline Code39Ring::Code39Ring () {
  for(uint8_t l = 12; l--; ) {
    m_displayBuffer[l] = BarcodeSymbol[0x11];
  }
}

inline void Code39Ring::display(uint8_t code[11])
{
  
  m_displayBuffer[0] = BarcodeSymbol[0x10];
  
  for (uint8_t i = 0; i < 10; i++) {
    m_displayBuffer[i + 1] = BarcodeSymbol[(code[i]) % 0x10];
  }
  
  m_displayBuffer[11] = BarcodeSymbol[0x10];
}

inline uint8_t Code39Ring::get(uint32_t loopCount)
{  
  uint16_t placement = loopCount % 0x100; 
  
  if (placement > 0xC0) return 0; // Each symbol is 16 wide. There are 12 incl. asterisks.
  
  uint8_t charPos = placement >> 4;
  uint8_t bitPos = 0x10  - (placement % 0x10);
  
  uint8_t byteDisp = (m_displayBuffer[charPos] >> bitPos) & 1;
  
  if (byteDisp) return 1;
  else return 0;
}

#endif // CODE39RING_h