/*
 * Copyright 2022 Alexander D Stokes
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#ifndef ECC_h
#define ECC_h

#include <unistd.h>
#include <stdint.h>

#include "GaloisField.hpp"

class ECC {
public:
  ECC();
  void harden(uint8_t code[11]);

private:
  
  uint8_t dataSymbols;
  uint8_t eccSymbols;

  uint8_t coefficients[8];
};


GaloisField field(0x10, 0x13);

inline ECC::ECC()
{ 
  
  dataSymbols = 6;
  eccSymbols = 4;

  memset(coefficients, 0, eccSymbols-1);
  coefficients[eccSymbols - 1] = 1;

  uint8_t root = 1; // field base 0
  for (uint8_t i = 0; i < eccSymbols; i++) {
    for (uint8_t j = 0; j < eccSymbols; j++) {
      coefficients[j] = field.multiply(coefficients[j], root);
      if (j + 1 < eccSymbols) {
        coefficients[j] ^= 
          coefficients[j + 1];
      }
    }

    root = field.multiply(root, 2);
  }
}


inline void ECC::harden(uint8_t code[11]) {

  uint8_t ecc[eccSymbols + dataSymbols];
  uint8_t eccOffset = 0;

  memset(ecc, 0, sizeof(ecc));

  for (uint8_t i = 0; i < dataSymbols; i++) {
    uint8_t factor = code[i] ^ ecc[eccOffset];
    eccOffset += 1;
    for (uint8_t j = 0; j < eccSymbols; j++) {
      ecc[j + eccOffset] ^= 
        field.multiply(coefficients[j], factor);
    }
  }

  for (uint8_t i = 0; i < eccSymbols; i++) {
    uint8_t cpt = ecc[eccOffset + i];
    code[i + 6] = cpt;
  } 
}

#endif // ECC_h