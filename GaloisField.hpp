/*
 * Copyright 2022 Alexander D Stokes
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef GALOISFIELD_h
#define GALOISFIELD_h

class GaloisField {
public:
  
  GaloisField(uint16_t size, uint16_t generator);
  uint8_t multiply(uint8_t x, uint8_t y);
  uint8_t invert(uint8_t x);
  uint8_t divide(uint8_t x, uint8_t y);
  uint8_t log(uint8_t x);
  uint8_t exp(uint8_t x);
  
  static uint8_t IsZero(uint8_t x);
  static uint8_t IsOne(uint8_t x);
  static uint8_t Add(uint8_t x, uint8_t y);
  
private:

  uint8_t gf_length;

  uint8_t gf_logTable[0x10];
  uint8_t gf_expTable[0x10];

};

inline GaloisField::GaloisField (uint16_t size, uint16_t generator) {
  gf_logTable[0] = 0;
  gf_length = size - 1;
  
  uint16_t j = 1;
  for (uint8_t i = 0; i < size; i++) {
    gf_expTable[i] = j;
    j = j << 1;
    if (j >= size) j ^= generator;
  }
  
  for (uint8_t i = 0; i < gf_length; i++) {
    gf_logTable[gf_expTable[i]] = i;
  }
  
  return;
}

inline uint8_t GaloisField::multiply(uint8_t x, uint8_t y) {
  if (x == 0 || y == 0) return 0;
  return gf_expTable[(gf_logTable[x] + gf_logTable[y]) % gf_length];
}

inline uint8_t GaloisField::invert(uint8_t x) {
  return gf_expTable[gf_length - gf_logTable[x]];
}

inline uint8_t GaloisField::divide(uint8_t x, uint8_t y) {
  return multiply(x, invert(y));
}

inline uint8_t GaloisField::log(uint8_t x) {
  return gf_logTable[x];
}

inline uint8_t GaloisField::exp(uint8_t x) {
  return gf_expTable[x];
}

inline uint8_t GaloisField::IsZero(uint8_t x) {
  return x == 0;
}

inline uint8_t GaloisField::IsOne(uint8_t x) {
  return x == 1;
}

inline uint8_t GaloisField::Add(uint8_t x, uint8_t y) {
  return x ^ y;
}

#endif // GALOISFIELD