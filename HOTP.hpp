/*
 * Copyright 2022 Alexander D Stokes
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HOTP_h
#define HOTP_h

#include <unistd.h>

#include "SHA1.hpp"
#include "V39.hpp"
#include "ECC.hpp"
#include "Base32.hpp"

#define BITS_PER_BASE32_CHAR      5
#define SHA1_DIGEST_LENGTH        20
#define HMAC_KEY_LENGTH           64
#define CHALLENGE_LENGTH          8
#define SECRET_MAX_LENGTH         120

class HOTP {
public:
  HOTP();

  void doSecret(uint8_t* configSlot, size_t length);
  void doChallenge(uint8_t* configSlot, size_t length);
  void digest(uint8_t code[7]);
  
private:
  V39 v39;

  uint8_t challenge[CHALLENGE_LENGTH];
  uint8_t secret[SECRET_MAX_LENGTH];
  size_t secretLen;
};


inline HOTP::HOTP()
{
  secretLen = 0;

  memset(secret, 0, sizeof(secret));
  memset(challenge, 0, sizeof(challenge));  
  
  v39 = V39();
}

inline void HOTP::doSecret(uint8_t* configSlot, size_t length)
{ 
  uint16_t buffer = 0;
  uint8_t sigBits = 0;
  uint16_t wroteBytes = 0;
  
  for (uint16_t i = 0; i < length; i++) {
    buffer <<= 5;
    buffer |= fromBase32(configSlot[i + 19]);
    sigBits += 5;
    if (sigBits >= 8) {
      secret[wroteBytes++] = buffer >> (sigBits - 8);
      sigBits -= 8;
    }
  }

  secretLen = wroteBytes;
}

inline void HOTP::doChallenge(uint8_t* configSlot, size_t length)
{
  v39.loadData(configSlot, 16);
  v39.increment();
  v39.dumpData(configSlot, 16);
  v39.toString(challenge);
}

inline void HOTP::digest(uint8_t code[7])
{ 
  SHA1 sha1;
  uint8_t sha[SHA1_DIGEST_LENGTH];
  uint8_t tmp_key[HMAC_KEY_LENGTH + CHALLENGE_LENGTH + HMAC_KEY_LENGTH];
  
  for (int i = 0; i < secretLen; ++i) {
    tmp_key[i] = secret[i] ^ 0x36;
  }
  
  if (secretLen < HMAC_KEY_LENGTH) {
    memset(tmp_key + secretLen, 0x36, HMAC_KEY_LENGTH - secretLen);
  }
  
  for (int i = 0; i < CHALLENGE_LENGTH; ++i) {
    tmp_key[HMAC_KEY_LENGTH + i] = challenge[i];
  }
  
  sha1.addBytes(tmp_key, HMAC_KEY_LENGTH + CHALLENGE_LENGTH);
  sha1.digest(sha);
  
  for (int i = 0; i < secretLen; ++i) {
    tmp_key[i] = secret[i] ^ 0x5C;
  }
  
  memset(tmp_key + secretLen, 0x5C, HMAC_KEY_LENGTH - secretLen);

  for (int i = 0; i < SHA1_DIGEST_LENGTH; ++i) {
    tmp_key[HMAC_KEY_LENGTH + i] = sha[i];
  }
  
  sha1.addBytes(tmp_key, HMAC_KEY_LENGTH + SHA1_DIGEST_LENGTH);
  sha1.digest(sha);

  uint8_t offset = sha[19] & 0xf;
  
  uint32_t result = (
    ((sha[offset] & 0x7f) << 24) |
    (sha[offset + 1] << 16) |
    (sha[offset + 2] << 8) |
    (sha[offset + 3])
   ) % 1000000;
      
  for (uint8_t l = 6; l--; ) {
    uint8_t digit = result % 10;
    result -= digit;
    result /= 10;
    
    code[l] = digit;
  }
  
  
  ECC ecc = ECC();
  
  ecc.harden(code);

}

#endif // HOTP_h
