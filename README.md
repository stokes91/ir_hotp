# IR HOTP

Small form factor security token firmware.

This includes a modification of the original RFC 4226 for AVR chips. This uses the HOTP algorithm, replacing the counter with a maxmial length linear feedback shift register. It also adds four symbols of error correction to the end of the original 6 digit OTP, which is encoded using code-39 and pulsed out as simply as the microcontroller allows.

## The Shift Register
LFSRs count using a shift register and some simple binary logic. All states, excluding one lockup state can be utilized. This uses a LFSR of 39 bits, with taps at 35 and 39. (Feedback polynomial of X^39 + X^35 + 1) Each increment does 5 iterations, to reduce the number of writes to EEPROM.

## Reed Solomon Error Correction
Four symbols are added to the end for error correction. If a mis-read is detected by a reader, a couple characters can be recovered, or the message discarded. This is to limit the likelihood a mis-read causes a lock-out, while also enabling readers to be more reliable

## Power-up sequence

```

Setup
- Read EEPROM
- Increment LFSR (5x)
- Write EEPROM (shift start by one)
- Generate OTP
- Encode barcode

Loop
- Continuously transmit barcode

(Safely brown-out to power loss)

```