/*
 * Copyright (C) 2011 Google Inc. All rights reserved.
 * Copyright (C) 2015 Apple Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SHA1h
#define SHA1h

inline uint32_t rotateLeft(int n, uint32_t x)
{
  return (x << n) | (x >> (32 - n));
}

#define message(t) (w[t]  = (buffer[t*4 + 0] << 24) \
    | (buffer[t*4 + 1] << 16) \
    | (buffer[t*4 + 2] << 8) \
    | (buffer[t*4 + 3]))

#define stretchMessage(t) (w[t&15] = rotateLeft(1, w[(t+13)&15]^w[(t+8)&15]^w[(t+2)&15]^w[t&15]))

class SHA1 {
public:
  SHA1();

  void addBytes(const uint8_t* input, size_t length);
  void digest(uint8_t digest[20]);

private:
  void processBlock();
  void reset();

  uint8_t buffer[64];
  size_t cursor;
  uint64_t totalBytes;
  uint32_t hash[5];
};

inline SHA1::SHA1()
{
  reset();
}

inline void SHA1::reset()
{
  cursor = 0;
  totalBytes = 0;
  hash[0] = 0x67452301;
  hash[1] = 0xefcdab89;
  hash[2] = 0x98badcfe;
  hash[3] = 0x10325476;
  hash[4] = 0xc3d2e1f0;
  
  memset(buffer, 0, sizeof(buffer));
}

inline void SHA1::addBytes(const uint8_t* input, size_t length)
{
  while (length--) {
    buffer[cursor++] = *input++;
    ++totalBytes;
    if (cursor == 64)
      processBlock();
  }
}

inline void SHA1::digest(uint8_t digest[20])
{
  buffer[cursor++] = 0x80;
  if (cursor > 56) {
    while (cursor < 64)
      buffer[cursor++] = 0x00;
    processBlock();
  }

  for (size_t i = cursor; i < 56; ++i)
    buffer[i] = 0x00;

  uint64_t bits = totalBytes * 8;
  for (size_t i = 0; i < 8; ++i) {
    buffer[56 + (7 - i)] = bits & 0xFF;
    bits >>= 8;
  }
  cursor = 64;
  processBlock();
    
  for (size_t i = 5; i--; ) {
    for (size_t j = 4; j--;) {
      digest[4 * i + j] = hash[i] & 0xFF;  
      hash[i] >>= 8;
    }
  }
  
  reset();
}

inline void SHA1::processBlock() 
{
  uint32_t w[16];
  
  uint32_t a = hash[0];
  uint32_t b = hash[1];
  uint32_t c = hash[2];
  uint32_t d = hash[3];
  uint32_t e = hash[4];

  for (size_t t = 0; t < 80; ++t) {
    
    uint32_t f;
    uint32_t k;

    if (t<20) {
        f = (b & c) | (~b & d);
        k = 0x5A827999;
    } else if (t<40) {
        f = b ^ c ^ d;
        k = 0x6ED9EBA1;
    } else if (t<60) {
        f = (b & c) | (b & d) | (c & d);
        k = 0x8F1BBCDC;
    } else {
        f = b ^ c ^ d;
        k = 0xCA62C1D6;
    }
        
    uint32_t temp = rotateLeft(5, a) + f + e + k;
    e = d;
    d = c;
    c = rotateLeft(30, b);
    b = a;

    if (t < 16) {
      a = temp + message(t);
    } else {
      a = temp + stretchMessage(t);
    }
  }

  hash[0] += a;
  hash[1] += b;
  hash[2] += c;
  hash[3] += d;
  hash[4] += e;

  cursor = 0;
}

#endif // SHA1h