/*
 * Copyright 2022 Alexander D Stokes
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef V39_h
#define V39_h

#define RBE_SLOT_SIZE 16

#include "Base32.hpp"

class V39 {
public:
  V39();

  void increment();
  void toString(uint8_t *result);
  
  void loadData(uint8_t* configSlot, size_t length);
  void dumpData(uint8_t* configSlot, size_t length);
  
private:
  uint32_t lowBits;
  uint32_t highBits;
  uint16_t start;

  uint8_t ringBuffer[RBE_SLOT_SIZE];
};

inline V39::V39()
{
  lowBits = 0;
  highBits = 1 << 3;
  start = 0;

  memset(ringBuffer, 0x41, sizeof(ringBuffer)); // 'A'
  ringBuffer[0] = 0x62; // 'b'
}

inline void V39::increment()
{ 
  uint32_t bit = 0;
  
  for (size_t l = 5; l--;) {
    // 39 35

    bit = ((lowBits >> 0) ^ (lowBits >> 4)) & 1;

    lowBits = (((highBits & 1) << 31) | (lowBits >> 1)) >> 0;
    highBits = (bit << 6) | (highBits >> 1);
  }
  
  uint8_t hi = getBase32((uint8_t) ((highBits >> 3) & 0x1f)) | 0x20;
  uint8_t lo = getBase32((uint8_t) (((highBits << 2) | (lowBits >> 30))  & 0x1f));
  
  ringBuffer[start] = lo;
  start += RBE_SLOT_SIZE - 1;
  start %= RBE_SLOT_SIZE;

  ringBuffer[start] = hi;
}

inline void V39::toString(uint8_t *result)
{
 
  uint8_t r[8] = {
    (uint8_t) ((highBits >> 3) & 0x1f),
    (uint8_t) (((highBits << 2) | (lowBits >> 30))  & 0x1f),
    (uint8_t) ((lowBits >> 25) & 0x1f),
    (uint8_t) ((lowBits >> 20) & 0x1f),
    (uint8_t) ((lowBits >> 15) & 0x1f),
    (uint8_t) ((lowBits >> 10) & 0x1f),
    (uint8_t) ((lowBits >> 5) & 0x1f),
    (uint8_t) ((lowBits) & 0x1f),
  };

  for (size_t i = 0; i < 8; i++) {
    result[i] = getBase32(r[i]);
  }
  
  // Lower case the first letter.
  result[0] |= 0x20; 
  
}

inline void V39::loadData(uint8_t* configSlot, size_t length) {
  uint8_t chr;
  
  for (uint8_t i = 0; i < length; i++) {
    ringBuffer[i] = configSlot[i];
    if (configSlot[i] > 0x60) {     // Found first lower case
      start = i;
    }
  }
  
  uint8_t innerStart = start;
  for (uint8_t i = 0; i < 8; i++) {
    
    uint8_t j = ringBuffer[innerStart++];
    innerStart %= length;
    
    highBits = ((highBits & 0x3) << 5) | (lowBits >> 27); // last 2 preserved. 32-27 = 5
    lowBits = (lowBits << 5) | fromBase32(j);
  }
}

inline void V39::dumpData(uint8_t* configSlot, size_t length) {
  for(uint16_t l = 0; l < length; l++) {
   configSlot[l] =  ringBuffer[l];
  }
}

#endif // V39_h