/*
 * Copyright 2022 Alexander D Stokes
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <avr/wdt.h>
#include <EEPROM.h>

#include "Code39Ring.hpp"
#include "HOTP.hpp"

#define RECORD_MAX_LENGTH         120

uint32_t loopCount = 0;

Code39Ring code39Ring;

// The code for an example OTP is only 6 characters long. Four extras are used
// to error correct.

uint8_t code[10];

HOTP hotp = HOTP();
  
void setup() {
  wdt_disable();
  
  DDRB = 1;

  // Attempt to read from EEPROM
  // if an invalid character is 
  // reached, stop.

  uint8_t t;
  uint8_t eepromData[RECORD_MAX_LENGTH];
  
  uint16_t messageLen = 0;
  for (uint8_t i = 0; i < 3; i++) {
    uint8_t chr = EEPROM.read(i + 16);    // Skip over challenge
    if (chr > '9' || chr < '0') continue;
    messageLen *= 10;
    messageLen += (chr - '0') % 10;
  }

  messageLen += 19;
  
  for (uint8_t i = 0; i < messageLen; i++) {
    t = EEPROM.read(i);
    if ( ( t > 0x2f && t < 0x3a ) || ( t > 0x40 && t < 0x5b) || ( t > 0x60 && t < 0x7b)) {
      eepromData[i] = t;
      continue;
    }
    break;
  }

  hotp.doSecret(eepromData, messageLen);
  hotp.doChallenge(eepromData, messageLen);
  hotp.digest(code);
  
  for (uint8_t i = 0; i < messageLen; i++) {
    EEPROM.update(i, eepromData[i]);
  }

  code39Ring.display(code);
}

void loop() {
  
  PORTB = code39Ring.get(++loopCount);
}
